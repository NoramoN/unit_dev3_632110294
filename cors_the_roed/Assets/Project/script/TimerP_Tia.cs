﻿using System;
using Samarnggoon.GameDev3.Chapter6.UnityEvents;
using TMPro;
using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter4
{
    public class TimerP_Tia : MonoBehaviour
    {
        [SerializeField] private GameObject _player;
        [SerializeField] private float time_count;
        [SerializeField] private GenericInteractable _genericInteractable;
        [SerializeField] private TextMeshProUGUI timetext;
        private float current_time = 0;

        private void Start()
        {
            ReStartTime();
        }

        private void Update()
        {
            if (current_time > 0)
            {
                current_time -= Time.deltaTime;
                print("Time current : " + current_time);
                timetext.text = ((int)current_time).ToString();
            }
            else
            {
                EndTime();
            }
        }

        public void SetZero()
        {
            current_time = time_count;
            timetext.text = "10";
            ReStartTime();
            print("SET ZEROOOOOOOOOOOOOOOOOO");
        }

        public void ReStartTime()
        {
            current_time = time_count;
        }

        public void EndTime()
        {
            print("Time Enddddd!!!!!!!!!");
            _genericInteractable.Interact(_player);
        }
    }
}