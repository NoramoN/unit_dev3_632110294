using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Carcontrol : MonoBehaviour
{
    private Rigidbody rb;
    public float speed = 1.0f;
    public float minspeed = 8.0f;
    public float maxspeed = 12.0f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(minspeed, maxspeed);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 forward = transform.up ; // x = right , y = up, z = forward
        rb.MovePosition(rb.position + forward * Time.deltaTime * speed);  
    }
}