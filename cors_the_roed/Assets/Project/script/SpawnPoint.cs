using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpawnPoint : MonoBehaviour
{
    public GameObject carPrefab;
    public float delay = 0.8f;
    public float nextTime;

    private void Start()
    {
      
    }

    private void Update()
    {
        if (nextTime <= Time.time)
        {
            spawnCar(); // สร้างรถ
            nextTime = Time.time + delay; // คูลดาว
        }
    }

    void spawnCar()
    {
        Instantiate(carPrefab, transform.position, transform.rotation); // Instantiate เรียกใช้ให้รถเกิดซ้ำห
    }
}