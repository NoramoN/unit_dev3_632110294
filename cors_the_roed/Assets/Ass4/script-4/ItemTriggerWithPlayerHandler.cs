using System;
using UnityEngine;
namespace Samarnggoon.GameDev3.Chapter4
{
    public class ItemTriggerWithPlayerHandler : MonoBehaviour
    {
        [SerializeField] private bool take_object = true;
        [SerializeField] private TimerP_Tia _timerPTia;
        protected virtual void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //Get the Inventory component from the player
                var inventory = other.GetComponent<Inventory>();
                //Add the collected item�s tag name to the invento
                if (take_object)
                {
                    inventory.AddItem(true);
                    Destroy(gameObject);
                }
                else
                {
                    if (inventory.is_teke_malai)
                    {
                        Destroy(gameObject);
                        _timerPTia.SetZero();
                    }

                    inventory.AddItem(false);
                    
                }
                
                
            }
        }

        protected virtual void ProcessTriggerWithDiamond()
        {

        }
        protected virtual void ProcessTriggerWithCoin()
        {

        }
    }
}