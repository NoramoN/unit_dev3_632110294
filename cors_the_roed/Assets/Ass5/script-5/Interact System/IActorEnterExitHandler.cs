using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter5.InteractionSystem
{
    public interface IActorEnterExitHandler
    {
        void ActorEnter(GameObject actor);
        void ActorExit(GameObject actor);
    }
}