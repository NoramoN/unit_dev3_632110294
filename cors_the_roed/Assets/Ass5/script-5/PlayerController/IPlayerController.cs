using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter5.PlayerController
{
    public interface IPlayerController
    {
        void MoveForward();
        void MoveForwardSprint();
        
        void MoveBackward();
        
        void TurnLeft();
        void TurnRight();
    }
}
public class IPlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

